User.create!(name:  "Marjorie O'Rourke",
             email: "marjie@gmail.com",
             password:              "foobar",
             password_confirmation: "foobar",
             admin: true)
             
Product.delete_all
Product.create! id: 1, name: "Banana", description: "a product", price: 0.49, active: true
Product.create! id: 2, name: "Apple", description: "a product", price: 0.29, active: true
Product.create! id: 3, name: "Carton of Strawberries", description: "a product", price: 1.99, active: true

OrderStatus.delete_all
OrderStatus.create! id: 1, name: "In Progress"
OrderStatus.create! id: 2, name: "Placed"
OrderStatus.create! id: 3, name: "Shipped"
OrderStatus.create! id: 4, name: "Cancelled"