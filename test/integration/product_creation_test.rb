require 'test_helper'

class ProductCreationTest < ActionDispatch::IntegrationTest
 
 test "invalid product information" do
   get newlisting_path
   assert_no_difference 'Product.count' do
   post newlisting_path, params: { product: { name: " ",
                                                  price: 0.00,
                                                  description: " " } }
   end
   assert_template 'products/new'
 end
 
 # After seed you should try to create a product to make sure it redirects to @product
 test "valid product information" do
  get newlisting_path
  assert_difference 'Product.count', 1 do
   post newlisting_path, params: { product: { name: "Example Product",
                                                  price: 35.00,
                                                  description: "An example product" } }
   end
   assert_template @product
  end
 
end
