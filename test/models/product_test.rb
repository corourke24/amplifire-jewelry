require 'test_helper'

class ProductTest < ActiveSupport::TestCase
  
  def setup
    @product = Product.new(name: "Example Product", price: 35.50, 
                           description: "An example product")
  end
  
  test "prodcut should be valid" do
    assert @product.valid?
  end
  
  test "product should have name" do
    @product.name = ""
    assert_not @product.valid?
  end
  
  test "prodcut should have price" do
    @product.price = ""
    assert_not @product.valid?
  end
  
  test "product should have description" do
    @product.description = ""
    assert_not @product.valid?
  end
  
end
