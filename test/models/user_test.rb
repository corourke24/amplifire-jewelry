require 'test_helper'

class UserTest < ActiveSupport::TestCase
  
  def setup
    @user = User.new(name: "Example", email: "user@example.com",
                     password: "examplepass", 
                     password_confirmation: "examplepass")
  end
  
  test "user should be valid" do
    assert @user.valid?
  end
  
  test "user name should be valid" do
    @user.name = "    "
    assert_not @user.valid?
  end
  
  test "user email should be present" do
    @user.email = "    "
    assert_not @user.valid?
  end
  
  test "name shouldn't be too long" do
    @user.name = "a" * 51
    assert_not @user.valid?
  end
  
  test "email shouldn't be too long" do 
    @user.email = "a" * 244 + "@example.com"
    assert_not @user.valid?
  end

  test "validation should accept valid email addresses" do
    valids = %w[user@example.com new_user-1@foo.bar.com USER@bar.org
                us+er@bar.cn user.name@foobar.jp]
    valids.each do |valid|
      @user.email = valid
      assert @user.valid?, "#{valid.inspect} should be a valid address"
    end
  end
  
  test "validation should reject invalid email addresses" do
    invalids = %w[user@example,com user_at_example.com user@example.
                  user@foo_bar.org user@foo+bar.com]
    invalids.each do |invalid|
      @user.email = invalid
      assert_not @user.valid?, "#{invalid.inspect} should not be a valid address"
    end
  end
  
  test "email addresses should be unique" do
    dup_user = @user.dup
    dup_user.email = @user.email.upcase
    @user.save
    assert_not dup_user.valid?
  end
  
  test "email address should be saved downcase" do
    new_email = "UsEr@EXamPLe.CoM"
    @user.email = new_email
    @user.save
    assert_equal new_email.downcase, @user.reload.email
  end
   
  test "password should be present" do
    @user.password = @user.password_confirmation = " " * 6
    assert_not @user.valid?
  end
  
  test "password should be at least 6 characters" do
    @user.password = @user.password_confirmation = "a" * 5
    assert_not @user.valid?
  end
    
  test "authenticated? method should return false for user with nil digest" do
    assert_not @user.authenticated?('')
  end
end
