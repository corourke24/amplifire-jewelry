require 'test_helper'

class ApplicationHelperTest < ActionView::TestCase
    
    test "full title helper" do
        assert_equal full_title,         "AmpliFire Energetics"
        assert_equal full_title("Help"), "AmpliFire Energetics | Help"
    end
    
end