class Product < ApplicationRecord
    has_many :order_items
    #default_scope -> { where(active: true).order(created_at: desc)  }
    default_scope { where(active: true) }
    mount_uploader :picture, PictureUploader
    validates :name,  presence: true, length: { maximum: 300 }
    PRICE_REGEXP = /\A\d{1,4}(\.\d{0,2})?\z/
    validates :price, presence: true,
                      numericality: true,
                      format: { with: PRICE_REGEXP }
    validates :description, presence: true, length: { maximum: 1000 }, 
                            allow_nil: true
    # Unsure as to whether this will work, or mess up individual product pages when not viewing as index
    # Also need to add this part to the default_scope defined above
    #default_scope -> { order(created_at: desc) }
    validate :picture_size

    private
    
        def picture_size
            if picture.size > 5.megabytes
                errors.add(:picture, "must be less than 5MB")
            end
        end
end
