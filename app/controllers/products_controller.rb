class ProductsController < ApplicationController
  before_action :logged_in?, only: [:create, :destroy]
  
  def show
    @product = Product.find(params[:id])
  end
  
  def new
    @product = Product.new
  end
  
  def create
    @product = Product.new(product_params)
    if @product.save
      redirect_to @product
    else
      render 'new'
    end
  end
  
  def index
    @products = Product.paginate(page: params[:page])
    @order_item = current_order.order_items.new
  end
  
  def destroy
    @product = Product.find(params[:id])
    @product.destroy
    flash[:success] = "Product deleted"
    redirect_to products_path
  end
  
  private
  
    def product_params
      params.require(:product).permit(:name, :price, :description, :picture)
    end
end